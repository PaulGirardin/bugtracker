class CommentsController < ApplicationController
    before_action :set_comment, only: [:show, :edit, :update, :destroy]

    def index
        @comments = Comment.all
    end
    
    def create
        @bug = Bug.find(params[:bug_id])
        @comment = @bug.comments.create(comment_params)
        redirect_to bugs_path(@bug)
    end
    
    def destroy
        @bug = Bug.find(params[:bug_id])
        @comment = @bug.comments.find(params[:id])
        @comment.destroy
        redirect_to bugs_path(@bug)
    end
    
    def delete
        @comment = Comment.find(params[:bug_id])
        @comment.destroy
        redirect_to comments_path
    end
    
    def edit
    end
    
    private
        # Use callbacks to share common setup or constraints between actions.
        def set_comment
          @comment = Comment.find(params[:id])
        end
    
        def comment_params
            params.require(:comment).permit(:commenter, :body)
        end
end
