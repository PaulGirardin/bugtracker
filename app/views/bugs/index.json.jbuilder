json.array!(@bugs) do |bug|
  json.extract! bug, :id, :name, :effect
  json.url bug_url(bug, format: :json)
end
